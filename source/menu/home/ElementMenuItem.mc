using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class ElementMenuItem extends DataMenuItem{
	
	function initialize (id, data) {
		DataMenuItem.initialize(id, data.name,data);
	}
		
	function onUpdate (dc) {
		if(models.hasKey(data.name) && models.get(data.name).status == :Work){
			DataMenuItem.setValue(Utils.timeToString(models.get(data.name).counter));
		}else{
			DataMenuItem.setValue(Utils.timeToString(data.time)); 
		}
	}
}