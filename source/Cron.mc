using Toybox.Timer;
using Toybox.Lang;

// utility class to wrap Toybox.Timer so that
// we can register and run timers but avoid "too many timers" errors.
var _cron;
class Cron {
	static const CHECK_LOOP_INTERVAL_MS = 100;
	protected var t;
	protected var jobs;
	
	static function getInstance() {
		if (_cron == null) {
			_cron = new Cron();
		}
		return _cron;
	}

	function initialize() {
		self.jobs = {};
    	self.t = new Timer.Timer();
	}
	
	function register(timerName, everyMs, callback, repeat) {
		if(self.jobs.isEmpty()){
			self.t.start(self.method(:checkTimersLoop), CHECK_LOOP_INTERVAL_MS, true);
		}
		if (self.jobs.hasKey(timerName)) {
			System.println(Lang.format("timer already registered with name $1$", [timerName]));
			return;
		} else if (everyMs < CHECK_LOOP_INTERVAL_MS) {
			System.println(Lang.format("timer loop must be at least $1$ milliseconds", CHECK_LOOP_INTERVAL_MS));
			return;
		} else if (!(callback instanceof Lang.Method)) {
			System.println("callback must be an invokeable method instance");
			return;
		}
		
		if (everyMs % CHECK_LOOP_INTERVAL_MS != 0) {
			System.println(Lang.format("Job repeat time $1$ doesn't schedule roundly with cron loop; check config for job=$2$", [everyMs, timerName]));
		}

		self.jobs.put(timerName, {
			:enabled => true,
			:every => everyMs,
			:sinceLast => 0,
			:callback => callback,
			:repeat => repeat
		});		
	}
	
	function isRegistered(timerName) {
		return self.jobs.hasKey(timerName);
	}
	
	function isEnabled(timerName) {
		return self.isRegistered(timerName) && self.jobs.get(timerName)[:enabled];
	}
	
	function enable(timerName) {
		if (!self.jobs.hasKey(timerName)) {
			System.println(Lang.format("no such timer found with name $1$", [name]));
			return;
		}
		self.jobs[timerName][:enabled] = true;
	}

	function disable(timerName) {
		if (!self.jobs.hasKey(timerName)) {
			System.println(Lang.format("no such timer found with name $1$", [name]));
			return;
		}
		self.jobs[timerName][:enabled] = false;
	}
	
	function unregister(timerName) {
		self.jobs.remove(timerName);
		if(self.jobs.isEmpty()){
			t.stop();
		}
	}
	
	function checkTimersLoop() {
		var jobNames = self.jobs.keys();
		for (var i = 0; i < jobNames.size(); i++) {
			var jobName = jobNames[i];
			var job = self.jobs[jobName];
			job[:sinceLast] += CHECK_LOOP_INTERVAL_MS;
			if (!job[:enabled] || job[:sinceLast] < job[:every]) {
				continue;
			}
			
			job[:callback].invoke();
			job[:sinceLast] = 0;
			
			if (job[:repeat]) {
				continue;
			}
			self.unregister(jobName);
		}
	}
}