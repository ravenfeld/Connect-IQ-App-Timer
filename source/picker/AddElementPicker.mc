using Toybox.WatchUi as Ui;

class AddElementPicker extends Ui.TextPickerDelegate {

    function initialize() {
        Ui.TextPickerDelegate.initialize();
    }

    function onTextEntered(text, changed) {
        elements.add(new Element(elements.size(),"{\"name\":\""+text+"\"}"));
    }

    function onCancel() {
        //Nothing
    }
}